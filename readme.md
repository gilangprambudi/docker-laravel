Dockerized Laravel Broilerplate


Just clone this repository and do <b>docker-compose up</b>, it will automatically run your laravel project inside docker container.
Perfect for isolated and portable development with laravel.